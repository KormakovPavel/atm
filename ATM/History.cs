﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATM
{
    class History
    {
        public int Id { get; set; }
        public DateTime DateOperation { get; set; }
        public string TypeOperation { get; set; }
        public int IdAccount { get; set; }
        public override string ToString()
        {
            return $"Дата операции: {DateOperation.ToString("dd.MM.yyyy")}, Тип операции: {TypeOperation}";
        }
    }
}
