﻿using System;
using System.Linq;

namespace ATM
{
    class Program
    {
        static void Main()
        {
            Mock mock = new Mock();
            Console.Write("Введите логин: ");
            string Login = Console.ReadLine();
            Console.Write("Введите пароль: ");
            string Password = Console.ReadLine(); 

            var queryUser = mock.Users.Where(f => f.Login == Login && f.Password == Password);
            if (queryUser.Count()!=0)
            {
                Console.WriteLine($"\nИнформация о пользователе:\n{queryUser.FirstOrDefault().ToString()}");
                var queryAccount = queryUser.ToArray().Join(mock.Accounts, u=> u.Id, a => a.IdUser, (u,a)=>new { a.Id,  Name = a.ToString() });
                Console.WriteLine($"\nИнформация о счетах:");
                if (queryAccount.Count() != 0)
                {
                    foreach (var account in queryAccount)
                        Console.WriteLine(account.Name);
                    var queryHistory = queryAccount.ToArray().Join(mock.Histories, a => a.Id, h => h.IdAccount, (a, h) => new { Name = $"Счет: {a.Name}; Действие: {h.ToString()}" });                    
                    if (queryHistory.Count() != 0)
                    {
                        Console.WriteLine($"\nИстория счетов:");
                        foreach (var history in queryHistory)
                            Console.WriteLine(history.Name);
                    }                        
                }
                else
                    Console.WriteLine("Счета не найдены");

            }                
            else
                Console.WriteLine("Пользователь не найден");

            var queryAllUser = mock.Users.Select(s=>s);
            if(queryAllUser.Count()!=0)
            {
                var queryAccount = queryAllUser.ToArray().Join(mock.Accounts, u => u.Id, a => a.IdUser, 
                                                               (u, a) => new { a.Id, Account = a.ToString(), Owner = $"{u.LastName} {u.FirstName} {u.Patronymic}" });
                if (queryAccount.Count() != 0)
                {
                    var queryHistory = queryAccount.ToArray().Join(mock.Histories.Where(w=>w.TypeOperation== "Зачисление средств"), a => a.Id, h => h.IdAccount, 
                                                                   (a, h) => new { Name = $"Операция: {h.ToString()}; Владелец: {a.Owner}" });
                    if (queryHistory.Count() != 0)
                    {
                        Console.WriteLine("\nИнформация об операциях по зачислению средств всех пользователей:");
                        foreach (var history in queryHistory)
                            Console.WriteLine(history.Name);
                    }                        
                }                    
            }

            Console.Write("\nВведите сумму: ");
            Int32.TryParse(Console.ReadLine(), out int Sum);
            if(Sum!=0 && queryAllUser.Count() != 0)
            {
                var queryAccount = queryAllUser.ToArray().Join(mock.Accounts.Where(w=>w.SumAccount>Sum), u => u.Id, a => a.IdUser,
                                                               (u, a) => new { Name = $"Владелец: {u.LastName} {u.FirstName} {u.Patronymic}, Сумма: {a.SumAccount}" });
                if (queryAccount.Count() != 0)
                {
                    Console.WriteLine($"\nПользователи, у которых сумма на счету больше {Sum}:");
                    foreach (var account in queryAccount)
                        Console.WriteLine(account.Name);
                }
            }
            else
                Console.WriteLine("Счета не найдены");

            Console.ReadLine();
        }
    }
}
