﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATM
{
    class Mock
    {
        public List<User> Users = new List<User>()
        {
            new User(){ Id = 1, FirstName = "Иван", LastName = "Федорчук", Patronymic = "Александрович", Phone = "89143514509",
                        Passport="8008 141298", DateRegistrasion = new DateTime(2018,1,23), Login = "Ivan1984", Password = "qwerty" },
            new User(){ Id = 2, FirstName = "Кирилл", LastName = "Полях", Patronymic = "Максимович", Phone = "89247612365",
                        Passport="2345 982376", DateRegistrasion = new DateTime(2018,2,12), Login = "polyahkirill", Password = "Passw0rd" },
            new User(){ Id = 3, FirstName = "Данил", LastName = "Померанцев", Patronymic = "Сергеевич", Phone = "89999084256",
                        Passport="5612 097356", DateRegistrasion = new DateTime(2018,3,9), Login = "terminatort800", Password = "123456" },
             new User(){ Id = 4, FirstName = "Артур", LastName = "Атаманов", Patronymic = "Павлович", Phone = "89083497013",
                        Passport="6734 096286", DateRegistrasion = new DateTime(2018,4,27), Login = "ataman777", Password = "qazwsx" },
               new User(){ Id = 5, FirstName = "Ирина", LastName = "Гутман", Patronymic = "Рудольфовна", Phone = "89159083271",
                        Passport="7732 563051", DateRegistrasion = new DateTime(2018,7,30), Login = "kisa1990", Password = "asdfgh" },
        };

        public List<Account> Accounts = new List<Account>()
        {
            new Account(){Id=1,DateOpen=new DateTime(2018,1,23),SumAccount=1000,IdUser=1},
            new Account(){Id=2,DateOpen=new DateTime(2018,1,30),SumAccount=5000,IdUser=1},
            new Account(){Id=3,DateOpen=new DateTime(2018,2,12),SumAccount=10000,IdUser=2},
            new Account(){Id=4,DateOpen=new DateTime(2018,3,9),SumAccount=100000,IdUser=3},
            new Account(){Id=5,DateOpen=new DateTime(2018,4,20),SumAccount=50000,IdUser=3},
            new Account(){Id=6,DateOpen=new DateTime(2018,4,27),SumAccount=10000,IdUser=4},
            new Account(){Id=7,DateOpen=new DateTime(2018,4,29),SumAccount=2000,IdUser=4},
            new Account(){Id=8,DateOpen=new DateTime(2018,6,1),SumAccount=70000,IdUser=4}            
        };

        public List<History> Histories = new List<History>()
        {
            new History() { Id=1, DateOperation=new DateTime(2018, 1, 23), TypeOperation="Открытие счета", IdAccount=1 },
            new History() { Id=2, DateOperation=new DateTime(2018, 1, 23), TypeOperation="Зачисление средств", IdAccount=1 },
            new History() { Id=3, DateOperation=new DateTime(2018, 1, 25), TypeOperation="Снятие наличных", IdAccount=1 },
            new History() { Id=4, DateOperation=new DateTime(2018, 1, 30), TypeOperation="Открытие счета", IdAccount=2 },
            new History() { Id=5, DateOperation=new DateTime(2018, 1, 30), TypeOperation="Зачисление средств", IdAccount=2 },
            new History() { Id=6, DateOperation=new DateTime(2018, 2, 12), TypeOperation="Открытие счета", IdAccount=3 },
            new History() { Id=7, DateOperation=new DateTime(2018, 2, 13), TypeOperation="Зачисление средств", IdAccount=3 },
            new History() { Id=8, DateOperation=new DateTime(2018, 3, 9), TypeOperation="Открытие счета", IdAccount=4 },
            new History() { Id=9, DateOperation=new DateTime(2018, 3, 11), TypeOperation="Зачисление средств", IdAccount=4 },
            new History() { Id=10, DateOperation=new DateTime(2018, 3, 15), TypeOperation="Перевод средств", IdAccount=4 },
            new History() { Id=11, DateOperation=new DateTime(2018, 4, 20), TypeOperation="Открытие счета", IdAccount=5 },
            new History() { Id=12, DateOperation=new DateTime(2018, 4, 20), TypeOperation="Зачисление средств", IdAccount=5 },
            new History() { Id=13, DateOperation=new DateTime(2018, 4, 27), TypeOperation="Открытие счета", IdAccount=6 },
            new History() { Id=14, DateOperation=new DateTime(2018, 4, 29), TypeOperation="Зачисление средств", IdAccount=6 },
            new History() { Id=15, DateOperation=new DateTime(2018, 4, 29), TypeOperation="Открытие счета", IdAccount=7 },
            new History() { Id=16, DateOperation=new DateTime(2018, 4, 29), TypeOperation="Зачисление средств", IdAccount=7 },
            new History() { Id=17, DateOperation=new DateTime(2018, 6, 1), TypeOperation="Открытие счета", IdAccount=8 },
            new History() { Id=18, DateOperation=new DateTime(2018, 6, 5), TypeOperation="Зачисление средств", IdAccount=8 },
            new History() { Id=19, DateOperation=new DateTime(2018, 6, 15), TypeOperation="Перевод средств", IdAccount=8 }
        };
    }
}
