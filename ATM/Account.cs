﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATM
{
    class Account
    {
        public int Id { get; set; }        
        public DateTime DateOpen { get; set; }
        public int SumAccount { get; set; }
        public int IdUser { get; set; }
        public override string ToString()
        {
            return $"Дата открытия: {DateOpen.ToString("dd.MM.yyyy")}, Сумма: {SumAccount}";
        }
    }
}
